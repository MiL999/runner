//
//  GameViewController.swift
//  Runner
//
//  Created by Miłosz on 6/28/19.
//  Copyright © 2019 Miłosz Duda. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import GoogleMobileAds
import GameKit


class GameViewController: UIViewController, GADBannerViewDelegate {

	var adBannerView: GADBannerView!
	let LEADERBOARD_ID = "com.miloszduda.bouncyball.hiscore"
	var gcEnabled = Bool() // Check if the user has Game Center enabled
	var gcDefaultLeaderBoard = String() // Check the default leaderboardID
	
	
	
	
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let view = self.view as! SKView? {
            if let scene = SKScene(fileNamed: "GameScene") {
                view.presentScene(scene)
            }
            
            view.ignoresSiblingOrder = true
			self.authenticateLocalPlayer()
			
//          view.showsFPS = true
//          view.showsNodeCount = true
//			view.showsDrawCount = true
//			view.showsPhysics = true
			
			let y = 0

			if UIDevice.current.userInterfaceIdiom == .phone {
				let x = Int((self.view.frame.size.width / 2) - 160)
				adBannerView = GADBannerView(frame: CGRect(x: x, y: y, width: 320, height: 50))
			} else {
				let x = Int((self.view.frame.size.width / 2) - 234)
				adBannerView = GADBannerView(frame: CGRect(x: x, y: y, width: 468, height: 60))
			}
			adBannerView.delegate = self
			adBannerView.rootViewController = self
			adBannerView.adUnitID = "ca-app-pub-3440657459286885/5971679116"

			let reqAd = GADRequest()
		//	reqAd.testDevices = ["684287eb8f4a911e1bf240ca5a2148d1"]
		//	adBannerView.load(reqAd)
		//	view.addSubview(adBannerView)
			
			DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(15), execute: {
				self.adBannerView.load(reqAd)
				view.addSubview(self.adBannerView)
			})

        }
    }

	
	func adViewDidReceiveAd(_ bannerView: GADBannerView) {
		print("Banner loaded successfully")
	}
	
	func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
		print("Fail to receive ads")
		print(error)
	}
	

	
	
    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
//        if UIDevice.current.userInterfaceIdiom == .phone {
//            return .allButUpsideDown
//        } else {
            return .all
 //       }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
	
	

	
}






extension GameViewController: GKGameCenterControllerDelegate {
	
	
	
	func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
		gameCenterViewController.dismiss(animated: true, completion: nil)
	}
	
	func authenticateLocalPlayer() {
		let localPlayer: GKLocalPlayer = GKLocalPlayer.local
		//	UserDefaults.standard.set(true, forKey: "AuthenticationShown")
		
		localPlayer.authenticateHandler = {(ViewController, error) -> Void in
			if((ViewController) != nil) {
				// 1. Show login if player is not logged in
				self.present(ViewController!, animated: true, completion: nil)
			} else if (localPlayer.isAuthenticated) {
				// 2. Player is already authenticated & logged in, load game center
				self.gcEnabled = true
				
				// Get the default leaderboard ID
				localPlayer.loadDefaultLeaderboardIdentifier(completionHandler: { (leaderboardIdentifer, error) in
					if error != nil {
						print(error!)
					} else {
						self.gcDefaultLeaderBoard = leaderboardIdentifer!
					}
				})
				
			} else {
				// 3. Game center is not enabled on the users device
				self.gcEnabled = false
				print("Local player could not be authenticated!")
				print(error!)
			}
		}
	}
	
	
	func checkGCLeaderboard() {
		let gcVC = GKGameCenterViewController()
		gcVC.gameCenterDelegate = self
		gcVC.viewState = .leaderboards
		//gcVC.leaderboardIdentifier = LEADERBOARD_ID
		present(gcVC, animated: true, completion: nil)
	}
	
	
}
