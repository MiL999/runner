//
//  Player.swift
//  Runner
//
//  Created by Miłosz on 6/28/19.
//  Copyright © 2019 Miłosz Duda. All rights reserved.
//

import Foundation
import SpriteKit



enum PlayerState: UInt32 {
	case None = 0
	case GoingUp = 1
	case GoingDown = 2
	case Jumping = 4
	case Ducking = 8
	case Flying = 16
}



class Player {
	
	var playerState: UInt32 = PlayerState.None.rawValue
	var playerNode: PlayerNode?
	var jumpDistance: CGFloat?
	let light = SKLightNode()

	var x: CGFloat {
		get {
			return (playerNode?.position.x)!
		}
	}
	
	var y: CGFloat {
		get {
			return (playerNode?.position.y)!
		}
	}
	
	var startPosition: CGPoint {
		get {
			return playerNode!.startPosition
		}
	}
	
	
	
	init() {
		if(playerNode == nil) {
			playerNode = PlayerNode(self)
		}
	}
	
	func addToScene(_ scene: SKScene) {
		if let playerNode = playerNode {
			playerNode.position = startPosition
			scene.addChild(playerNode)
			light.position = startPosition
			light.falloff = 0.1
			light.ambientColor = .black
			light.lightColor = .lightGray
			light.categoryBitMask = 1
			//scene.addChild(light)
		}
	}
	
	
	
	
	// MARK: State
	
	func clearState() {
		self.playerState = PlayerState.None.rawValue
	}
	
	
	func startDucking() {
		if isJumping() {
			stopJumping()
		}
		
		self.playerState |= PlayerState.Ducking.rawValue
		self.playerNode?.duck()

	}
	
	
	func stopDucking() {
		self.playerState &= ~PlayerState.Ducking.rawValue
		self.playerNode?.stopDucking()
	}
	
	
	func startJumping() {
		if isDucking() {
			stopDucking()
		}
		
		self.playerState |= PlayerState.Jumping.rawValue
		self.playerNode?.jump()
	}

	
	func stopJumping() {
		self.playerState &= ~PlayerState.Jumping.rawValue
		self.playerNode?.setPlayerTexture(.Normal)
		
	}

	
	func isJumping() -> Bool {
		return ((self.playerState & PlayerState.Jumping.rawValue) > 0)
	}

	func isDucking() -> Bool {
		return ((self.playerState & PlayerState.Ducking.rawValue) > 0)
	}

	func isGoingUp() -> Bool {
		return ((self.playerState & PlayerState.GoingUp.rawValue) > 0)
	}

	func isGoingDown() -> Bool {
		return ((self.playerState & PlayerState.GoingDown.rawValue) > 0)
	}

	func isFlying() -> Bool {
		return ((self.playerState & PlayerState.Flying.rawValue) > 0)
	}

	
	func bounce() {
		self.playerNode?.bounce()
	}
	
	func startFlying() {
		self.playerState |= PlayerState.Flying.rawValue
		self.playerNode?.fly()
	}
	
	func stopFlying() {
		self.playerState &= ~PlayerState.Flying.rawValue
		self.playerNode?.stopFlying()
	}

	
}
