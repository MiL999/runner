//
//  Trap.swift
//  Runner
//
//  Created by Miłosz on 8/19/19.
//  Copyright © 2019 Miłosz Duda. All rights reserved.
//


import UIKit
import SpriteKit

class Trap: SKSpriteNode {
	
	
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		if let texture = self.texture {
			
			self.name = "Trap"
			
			var bodySize = self.size
			
			bodySize.height *= 0.92
			bodySize.width *= 0.96
			
			let nobody = self.userData?["nobody"] as? Int
			
			if nobody != 1 {
				self.physicsBody = SKPhysicsBody(texture: texture, alphaThreshold: 0.5, size: bodySize)
				if physicsBody == nil {
					assert(false, "Error creating trap physics body. \(String(describing: self.texture))")
				}
				self.physicsBody?.affectedByGravity = false
				self.physicsBody?.isDynamic = false
			}
			
			let swing = self.userData?["swing"] as? Int
			if let swing = swing {
				let rad = Double(swing) * .pi/180
				let action1 = SKAction.rotate(toAngle: CGFloat(rad), duration: 2.0)
				let action2 = SKAction.rotate(toAngle: -CGFloat(rad), duration: 2.0)
				action1.timingMode = .easeInEaseOut
				action2.timingMode = .easeInEaseOut
				let seq = SKAction.sequence([action1, action2])
				self.run(SKAction.repeatForever(seq))
			}
			
			let rotate = self.userData?["rotate"] as? Int
			if let rotate = rotate {
				let rad = Double(rotate) * .pi/180
				let action1 = SKAction.rotate(byAngle: CGFloat(rad), duration: 0.5)
				let seq = SKAction.sequence([action1])
				self.run(SKAction.repeatForever(seq))
			}
			
//			let move = self.userData?["move"] as? Int
//			if let move = move {
//				let rad = Double(self.zRotation) * .pi/180
//				let x = cos(rad) * 100
//				let y = sin(rad) * 100
//				let v = CGVector(dx: x, dy: y)
//				self.run(SKAction.move(by: v, duration: 5))
//			}
			
		}
	}
}


