//
//  LevelLayer.swift
//  Runner
//
//  Created by Miłosz on 7/23/19.
//  Copyright © 2019 Miłosz Duda. All rights reserved.
//

import Foundation
import SpriteKit


class LevelLayer {
	
	weak var scene: SKNode?
	
	var platforms = [SKTileMapNode]()
	var queue = [SKTileMapNode]()
	var platformIndex = 0
	var cameraPosPlatformTemp: Double = 0
	var numLevels = 10
	var currentLevels = [Int]()
	
	var levelWidth = 1536
	var speed: Double = 1.0
	var dy: CGFloat = 0.0
	
	
	init(_ scene: SKNode) {
		self.scene = scene
		
		platforms.reserveCapacity(numLevels)

		for l in (1...numLevels) {
			let name = "L\(l)"
			let level = scene.childNode(withName: name) as? SKTileMapNode
			platforms.append(level!)
		}

		queue.reserveCapacity(2)
		hideDummies()
		resetLevels()
	}
	
	
	
	func update() {
		cameraPosPlatformTemp += speed
		queue[0].position.x -= CGFloat(speed)
		queue[1].position.x -= CGFloat(speed)
		queue[0].position.y -= dy
		queue[1].position.y -= dy

		if Int(cameraPosPlatformTemp) > levelWidth - Int(speed) {
			cameraPosPlatformTemp = 0
			let l = randomLevel(not: currentLevels)
			queue[platformIndex].isHidden = true
			queue[platformIndex] = platforms[l]
			queue[platformIndex].isHidden = false
			currentLevels[platformIndex] = l
			queue[platformIndex].position = CGPoint(x: levelWidth, y: Int(queue[1 - platformIndex].position.y))
			platformIndex = 1 - platformIndex

			print("Levels: \(currentLevels[0]), \(currentLevels[1])")
		}
	}
	
	func randomLevel(not: [Int]) -> Int {
		var a = -1
		while not.contains(a) || a == -1 {
			a = Int.random(in: 1...numLevels-1)
		}
		return a
	}
	
	func hideDummies() {
		for p in platforms {
			p.enumerateChildNodes(withName: "Dummy", using: { (node, nil) in
				node.isHidden = true
			})
			
			p.enumerateChildNodes(withName: "DummyTrap", using: { (node, nil) in
				node.isHidden = true
			})
		}
	}
	
	
	func resetLevels() {
		queue.removeAll()
		currentLevels.removeAll()
		
		platforms.forEach {
			$0.position = CGPoint(x: levelWidth, y: 0)
			$0.isHidden = true
		}
		platforms[0].position = CGPoint(x: 0, y: 0)
		platforms[0].isHidden = false
		platforms[1].isHidden = false

		queue.insert(platforms[0], at: 0)
		queue.insert(platforms[1], at: 1)
		currentLevels.insert(0, at: 0)
		currentLevels.insert(1, at: 1)
		
		platformIndex = 0
		cameraPosPlatformTemp = 0
	}
	
	
	func blur() {
		
		let effectsNode = SKEffectNode()
		let filter = CIFilter(name: "CIGaussianBlur")
		let blurAmount = 10.0
		filter?.setValue(blurAmount, forKey: kCIInputRadiusKey)
		
		effectsNode.filter = filter
		effectsNode.position = self.scene!.position
		effectsNode.blendMode = .alpha
		
		
		
//		let sprite = background1?.children[0] as? SKSpriteNode
//		sprite?.removeFromParent()
//		effectsNode.addChild(sprite!)
//		scene?.addChild(effectsNode)
	}
	
	
	
}
