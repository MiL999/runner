//
//  BackgroundLayer.swift
//  Runner
//
//  Created by Miłosz on 7/23/19.
//  Copyright © 2019 Miłosz Duda. All rights reserved.
//

import Foundation
import SpriteKit

class BackgroundLayer {
	
	weak var scene: SKNode?
	
	var firstBackground: SKSpriteNode?
	
	var background1: SKTileMapNode?
	var background2: SKTileMapNode?
	var backgrounds = [SKTileMapNode]()
	var backgroundIndex = 0
	var cameraPosBkgTemp = 0.0
	var speed: Double = 1.0
	var dy: CGFloat = 0.0

	var backgroundWidth = 0
	var zShift: Int = 0 {
		didSet {
			backgrounds[0].position.y = CGFloat(zShift)
			backgrounds[1].position.y = CGFloat(zShift)
		}
	}
	
	
	init(_ scene: SKNode, b1: String, b2: String, zPosition: Int, width: Int) {
		self.scene = scene
		self.background1 = scene.childNode(withName: b1) as? SKTileMapNode
		self.background2 = scene.childNode(withName: b2) as? SKTileMapNode
		backgrounds.reserveCapacity(2)
		backgroundWidth = width
		
		if let b1 = background1, let b2 = background2 {
			b1.zPosition = CGFloat(zPosition)
			b2.zPosition = CGFloat(zPosition)
			backgrounds.append(b1)
			backgrounds.append(b2)
		}

		backgrounds[0].position = CGPoint(x: 0, y: 0)
		backgrounds[1].position = CGPoint(x: backgroundWidth, y: 0)
		
	}

	init(_ scene: SKNode, FirstBackground firstBackground: String) {
		self.firstBackground = scene.childNode(withName: firstBackground) as? SKSpriteNode
		self.firstBackground?.zPosition = -100
	//	createGradient()
	}
	
	
	func createGradient() {
		let c1 = SKColor.init(red: 140/255, green: 100/255, blue: 20/255, alpha: 1.0)
		let c2 = SKColor.init(red: 255/255, green: 236/255, blue: 170/255, alpha: 1.0)
		let texture = SKTexture.init(size: CGSize(width: 500, height: 500), startColor: c2, endColor: c1)
		firstBackground?.texture = texture
	}
	
	
	func update() {
		if let firstBackground = firstBackground {
			let pos = firstBackground.position.x
			if pos > 150 {
				var divider = 1.0
				if pos < 500 {
					divider = 2.0
				}
				firstBackground.position.x -= CGFloat(speed / divider)
			}
		} else {
			cameraPosBkgTemp += speed
			backgrounds[0].position.x -= CGFloat(speed)
			backgrounds[1].position.x -= CGFloat(speed)
			backgrounds[0].position.y -= dy
			backgrounds[1].position.y -= dy
			if Int(cameraPosBkgTemp) > backgroundWidth - Int(speed) {
				cameraPosBkgTemp = 0
				backgrounds[backgroundIndex].position = CGPoint(x: backgroundWidth, y: zShift + Int(backgrounds[1 - backgroundIndex].position.y))
				backgroundIndex = 1 - backgroundIndex
			}
		}
	}
	
	
	func resetBackground() {
		firstBackground?.position.x = 1070
	}
	
	func blur() {
		
		let effectsNode = SKEffectNode()
		let filter = CIFilter(name: "CIGaussianBlur")
		let blurAmount = 10.0
		filter?.setValue(blurAmount, forKey: kCIInputRadiusKey)
		
		effectsNode.filter = filter
		effectsNode.position = background1!.position
		effectsNode.blendMode = .alpha
		
		let sprite = background1?.children[0] as? SKSpriteNode
		sprite?.removeFromParent()
		effectsNode.addChild(sprite!)
		scene?.addChild(effectsNode)	
	}
	
	
	
}
