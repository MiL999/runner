//
//  Menu.swift
//  Runner
//
//  Created by Miłosz on 9/6/19.
//  Copyright © 2019 Miłosz Duda. All rights reserved.
//

import Foundation
import SpriteKit


class Menu {
	
	weak var scene: SKScene?
	var dimPanel: SKSpriteNode
	let start = SKSpriteNode(imageNamed: "Start")
	let leaderboard = SKSpriteNode(imageNamed: "leaderboard")
	let best = SKLabelNode(fontNamed: "Quango")
	var isInMenu = false
	
	
	
	init(_ scene: SKScene) {
		self.scene = scene
		let size = CGSize(width: (scene.size.width)+1500, height: (scene.size.height)+1500)
		dimPanel = SKSpriteNode(color: UIColor.black, size: size)
	}

	func showMenu() {
		let completion = {
			self.showButtons()
		}
		
		addDimPanel(fromAlpha: 1.0, toAlpha: 0.2, speed: 2.0, completion: completion)
		self.isInMenu = true
		SKTAudio.sharedInstance().playBackgroundMusic("music1.mp3", loops: 150)
		SKTAudio.sharedInstance().setMusicVolume(0.5)

	}
	
	
	
	func addDimPanel(fromAlpha: CGFloat?, toAlpha: CGFloat, speed: CGFloat, completion: (() -> Void)?) {
		if let fromAlpha = fromAlpha {
			dimPanel.alpha = fromAlpha
		}
		dimPanel.zPosition = 500
		dimPanel.position = CGPoint(x: ((scene?.size.width)!/2)+1, y: ((scene?.size.height)!/2)+1)
		dimPanel.removeFromParent()
		scene?.addChild(dimPanel)
		
		let seq = SKAction.sequence([SKAction.fadeAlpha(to: toAlpha, duration: TimeInterval(exactly: speed)!)])
		dimPanel.run(seq, completion: {
			completion?()
		})
	}
	
	
	
	func removeDimPanel(_ speed: Double) {
		dimPanel.run(SKAction.fadeOut(withDuration: TimeInterval(exactly: speed)! ), completion: {
			self.dimPanel.removeFromParent()
		})
	}
	
	
	func showButtons() {
		start.zPosition = 505
		start.position = CGPoint(x: (scene?.size.width)!/2, y: (scene?.size.height)!/2)
		start.size = CGSize(width: 200, height: 80)
		start.name = "start"
		start.alpha = 0
		start.removeFromParent()
		scene?.addChild(start)
		start.run(SKAction.fadeAlpha(to: 1, duration: 0.5))
		
		let hiScore = UserDefaults.standard.integer(forKey: "HiScore")
		best.text = "Best: \(hiScore)"
		best.zPosition = 505
		best.position = CGPoint(x: (scene?.size.width)!/2, y: -70 + (scene?.size.height)!/2)
		best.alpha = 0
		best.removeFromParent()
		scene?.addChild(best)
		best.run(SKAction.fadeAlpha(to: 1, duration: 0.5))

		leaderboard.zPosition = 505
		leaderboard.position = CGPoint(x: (scene?.size.width)! - 55, y: 50)
		leaderboard.size = CGSize(width: 80, height: 80)
		leaderboard.name = "leaderboard"
		leaderboard.alpha = 0
		leaderboard.removeFromParent()
		scene?.addChild(leaderboard)
		leaderboard.run(SKAction.fadeAlpha(to: 1, duration: 0.5))
		
		
	}
	
	func hideMenu() {
		let seq = SKAction.sequence([SKAction.scale(to: 0.95, duration: 0.05),
									 SKAction.fadeAlpha(to: 0.75, duration: 0.1),
									 SKAction.fadeOut(withDuration: 0.1)])
		
		start.run(seq, completion: {
			self.start.removeFromParent()
		})
		
		self.best.removeFromParent()
		self.leaderboard.removeFromParent()
		SKTAudio.sharedInstance().stopBackgroundMusic(1.0)

	}
	
	
	func showHelp() {
		let swipeUp = SKSpriteNode(imageNamed: "swipe_up")
		swipeUp.position = CGPoint(x: (scene?.size.width)!/2, y: 80 + (scene?.size.height)!/2)
		swipeUp.zPosition = 500
		swipeUp.size = CGSize(width: 80, height: 80)
		swipeUp.name = "help"
		scene?.addChild(swipeUp)
		
		let swipeDown = SKSpriteNode(imageNamed: "swipe_down")
		swipeDown.position = CGPoint(x: (scene?.size.width)!/2, y: -80 + (scene?.size.height)!/2)
		swipeDown.zPosition = 500
		swipeDown.size = CGSize(width: 80, height: 80)
		swipeDown.name = "help"
		scene?.addChild(swipeDown)

		let swipeUpLabel = SKLabelNode(fontNamed: "Quango")
		swipeUpLabel.text = "Jump"
		swipeUpLabel.position = CGPoint(x: 100 + (scene?.size.width)!/2, y: 60 + (scene?.size.height)!/2)
		swipeUpLabel.zPosition = 500
		swipeUpLabel.name = "help"
		scene?.addChild(swipeUpLabel)

		let swipeDownLabel = SKLabelNode(fontNamed: "Quango")
		swipeDownLabel.text = "Roll"
		swipeDownLabel.position = CGPoint(x: 100 + (scene?.size.width)!/2, y: -100 + (scene?.size.height)!/2)
		swipeDownLabel.zPosition = 500
		swipeDownLabel.name = "help"
		scene?.addChild(swipeDownLabel)
	}

	
	func hideHelp() {
		let helpItems = scene?.children.filter {
			$0.name == "help"
		}
		if let helpItems = helpItems {
			helpItems.forEach { node in
				node.removeFromParent()
			}
		}
		
		
	}
	
	
	
	
	
}
