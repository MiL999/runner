//
//  GameScene.swift
//  Runner
//
//  Created by Miłosz on 6/28/19.
//  Copyright © 2019 Miłosz Duda. All rights reserved.
//

import SpriteKit
import GameplayKit
import GameKit


class GameScene: SKScene  {

	let hiScoreLeaderboard = "com.miloszduda.bouncyball.hiscore"
	var mainCamera: SKCameraNode?
	lazy var player = Player()

	var firstBackground: BackgroundLayer?
	var backgroundLayer1: BackgroundLayer?
	var backgroundLayer2: BackgroundLayer?
	var backgroundLayer3: BackgroundLayer?
	var backgroundLayer0: BackgroundLayer?
	var levelLayer: LevelLayer?
	
	lazy var swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(jump))
	lazy var swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(duck))
	lazy var press = UILongPressGestureRecognizer(target: self, action: #selector(fly))
	
	var isDead = false
	var levelSpeed: Double = 5.0

	var previousTime: TimeInterval = 0
	var previousBounceTime: TimeInterval = 0

	var score: Double = 0 {
		didSet {
			scoreLabel?.text = "Score: \(Int(score))"
		}
	}
	
	var scoreLabel: SKLabelNode?
	var menu: Menu?

	var timeElapsed: Int64 = 0
	

	
	override func sceneDidLoad() {
		super.sceneDidLoad()
		
		print("W: \(self.size.width), H: \(self.size.height)")
		
		self.size.width = self.size.height * (UIScreen.main.bounds.size.width / UIScreen.main.bounds.size.height)
		self.scaleMode = .aspectFit
		
		print("W: \(self.size.width), H: \(self.size.height)")

	}
	
	
    override func didMove(to view: SKView) {
		self.isPaused = true
		self.isPaused = false
		
		//let referenceNode = self.childNode(withName: "Level1Reference") as? SKReferenceNode
		
		let resourcePath = Bundle.main.path(forResource: "Level1", ofType: "sks")
		let newLevel = SKReferenceNode (url: URL(fileURLWithPath: resourcePath!))
		newLevel.zPosition = 1
		self.addChild(newLevel)

		let sceneNode = newLevel.children[0]
		
		self.physicsWorld.contactDelegate = self
		
		menu = Menu(self)
		menu?.isInMenu = true
		
		self.view?.ignoresSiblingOrder = true
		
		
		firstBackground = BackgroundLayer(sceneNode, FirstBackground: "Sunset")
		firstBackground?.speed = 0.4

		levelLayer = LevelLayer(sceneNode)
		levelLayer?.levelWidth = 1536
		levelLayer?.speed = levelSpeed

		backgroundLayer0 = BackgroundLayer(sceneNode, b1: "B0", b2: "B00", zPosition: 200, width: 1920)
		backgroundLayer0?.speed = levelSpeed + 2

		swipeUp.direction = .up
		self.view?.addGestureRecognizer(swipeUp)
		swipeDown.direction = .down
		self.view?.addGestureRecognizer(swipeDown)

		addScoreLabel()
		menu?.showMenu()


		SKAction.playSoundFileNamed("bounce.wav", waitForCompletion: false)
		SKAction.playSoundFileNamed("jump.wav", waitForCompletion: false)
		SKAction.playSoundFileNamed("death.wav", waitForCompletion: false)
		SKAction.playSoundFileNamed("down.wav", waitForCompletion: false)
		SKAction.playSoundFileNamed("ambient.wav", waitForCompletion: false)

	//	SKTAudio.sharedInstance().playBackgroundMusic("music2.wav", loops: 150)
	//	SKTAudio.sharedInstance().setMusicVolume(0.4)

		self.isPaused = true
		self.isPaused = false

    }
	
	func addScoreLabel() {
//		var fonts = UIFont.familyNames
//		fonts.sort()
//		for name in fonts {
//			print(name)
////			if let nameString = name as? String {
////				print(UIFont.fontNames(forFamilyName: nameString))
////			}
//		}
		
		scoreLabel = SKLabelNode(fontNamed: "Quango")
		scoreLabel?.fontSize = 24
		scoreLabel?.position = CGPoint(x: self.frame.minX+30, y: self.frame.maxY - 40)
		scoreLabel?.zPosition = 205
		scoreLabel?.horizontalAlignmentMode = .left
		scoreLabel?.alpha = 0.0
		self.addChild(scoreLabel!)
	}

	


	

	
	
	func addController() {
		let x = self.frame.minX + 40
		let y = self.frame.minY + 40
		
		let leftArrow = SKSpriteNode(imageNamed: "arrowLeft")
		leftArrow.position = CGPoint(x: x, y: y)
		leftArrow.size = CGSize(width: 80, height: 80)
		leftArrow.name = "leftArrow"
		leftArrow.alpha = 0.7
		leftArrow.zPosition = 100
		mainCamera?.addChild(leftArrow)
		
		let rightArrow = SKSpriteNode(imageNamed: "arrowRight")
		rightArrow.position = CGPoint(x: x+80, y: y)
		rightArrow.size = CGSize(width: 80, height: 80)
		rightArrow.name = "rightArrow"
		rightArrow.alpha = 0.7
		rightArrow.zPosition = 100
		mainCamera?.addChild(rightArrow)
		
		let circle = SKShapeNode(circleOfRadius: 20)
		circle.position = CGPoint(x: self.frame.maxX - 40, y: y)
		circle.fillColor = .white
		circle.name = "jump"
		circle.alpha = 0.7
		circle.zPosition = 100
		mainCamera?.addChild(circle)
	}
	
	

	func startGame() {
		
		let completion2 = {
			self.menu?.removeDimPanel(1.0)
			self.menu?.isInMenu = false
			self.player.addToScene(self)
			self.score = 0
			self.scoreLabel?.alpha = 1.0
			self.setLevelSpeed(5.0)
			let sound = SKAction.playSoundFileNamed("ambient.wav", waitForCompletion: false)
			self.run(sound)

		}
		
		let completion1 = {
			self.levelLayer?.resetLevels()
			self.firstBackground?.resetBackground()
			self.menu?.addDimPanel(fromAlpha: nil, toAlpha: 0.0, speed: 1, completion: completion2)
		}
		
		let completionHelp = {
			self.menu?.showHelp()
			
			self.menu?.addDimPanel(fromAlpha: nil, toAlpha: 1.0, speed: 4, completion: {
				self.menu?.hideHelp()
				completion1()
			})

		}
		
		self.menu?.hideMenu()
		
		let hideHelp = UserDefaults.standard.integer(forKey: "HideHelp")
		if hideHelp == 0 {
			menu?.addDimPanel(fromAlpha: nil, toAlpha: 0.7, speed: 1, completion: completionHelp)
			UserDefaults.standard.set(1, forKey: "HideHelp")
		} else {
			menu?.addDimPanel(fromAlpha: nil, toAlpha: 1.0, speed: 1, completion: completion1)
		}
		
		
		

		
	}
	

	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		guard let touch = touches.first else {
			return
		}
		
		let touchPosition = touch.location(in: self)
		let touchedNodes = nodes(at: touchPosition)

		if let menu = menu, menu.isInMenu == true {
		
			for node in touchedNodes {
				switch(node.name) {
				case "start":
					self.startGame()
				case "leaderboard":
					self.checkLeaderboard()
				default:
					break
				}
			}
		
		
		}
		
		
	}
	

	
	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
		guard let touch = touches.first else {
			return
		}
		let previousLocation = touch.previousLocation(in: self)
		let location = touch.location(in: self)
		player.jumpDistance = (previousLocation.y - location.y) * -1

		
	}

	
	func checkLeaderboard() {
		
		if(!GKLocalPlayer.local.isAuthenticated) {
			let alertView = UIAlertController(title: "", message: "Please log in to Game Center in Settings -> Game Center.", preferredStyle: .alert)
			let cancel = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
			alertView.addAction(cancel)
			let currentViewController = UIApplication.shared.keyWindow?.rootViewController
			currentViewController?.present(alertView, animated: true, completion: nil)
		} else {
			let currentViewController = UIApplication.shared.keyWindow?.rootViewController as? GameViewController
			currentViewController?.checkGCLeaderboard()
		}
	}

	
	override func update(_ currentTime: TimeInterval) {
		let delta = currentTime - previousTime
		
		firstBackground?.update()
		backgroundLayer0?.update()
		levelLayer?.update()
		
		
		if isDead || menu!.isInMenu {
			return
		}
		
		if((player.playerNode?.physicsBody?.velocity.dy)! > CGFloat(0)) {
			player.playerState |= PlayerState.GoingUp.rawValue
			player.playerState &= ~PlayerState.GoingDown.rawValue
		} else {
			player.playerState |= PlayerState.GoingDown.rawValue
			player.playerState &= ~PlayerState.GoingUp.rawValue
		}
		

		if((ControllerManager.shared.controllerState & ControllerState.Jump.rawValue > 0) && !player.isJumping()) {
			player.playerNode?.physicsBody?.velocity = CGVector.zero
			player.playerNode?.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 30))
			ControllerManager.shared.clearState()
			let sound = SKAction.playSoundFileNamed("jump.wav", waitForCompletion: false)
			self.run(sound)
			player.startJumping()
		}

		if((ControllerManager.shared.controllerState & ControllerState.Duck.rawValue > 0) && !player.isDucking()) {
			player.playerNode?.physicsBody!.restitution = 0.3
			player.playerNode?.physicsBody?.applyImpulse(CGVector(dx: 0, dy: -10))
			ControllerManager.shared.clearState()
			let sound = SKAction.playSoundFileNamed("down.wav", waitForCompletion: false)
			self.run(sound)
			player.startDucking()
		}

//		if((ControllerManager.shared.controllerState & ControllerState.Fly.rawValue > 0) && player.isJumping() && !player.isFlying()) {
//			ControllerManager.shared.clearState()
//			player.startFlying()
//			self.setLevelSpeed(2)
//			self.physicsWorld.gravity = CGVector(dx: 0, dy: -3)
//		}

		
		
		updateVertivalMovement2()
		

		if (player.y < 0 || player.x < 0) && menu?.isInMenu != true {
			self.death()
		}

		if player.x < player.startPosition.x && delta > 0.1 {
			player.playerNode?.position.x += 1
		}

		player.playerNode?.physicsBody?.velocity.dx = 0

		player.light.position = (player.playerNode?.position)!
		

		
		score += 0.2
		
		if score == 200 {
			setLevelSpeed(6.0)
		}
		
		if score == 500 {
			setLevelSpeed(7.0)
		}
		
		
		let a = Int.random(in: 1...800)
		if a == 20 {
			SKTAudio.sharedInstance().playBackgroundMusic("birds.wav", loops: 0)
			print("birds")
		}
		
		
		
//		let time = Int64(Date().timeIntervalSince1970 * 1000) - timeElapsed
//		if Int(score) % 10 == 0 {
//			print("Update: \(time)")
//		}
//		timeElapsed = Int64(Date().timeIntervalSince1970 * 1000)
    }
	
	
//	override func didEvaluateActions() {
//		let time = Int64(Date().timeIntervalSince1970 * 1000) - timeElapsed
//		if Int(score) % 10 == 0 {
//			print("Evaluate actions: \(time)")
//		}
//		timeElapsed = Int64(Date().timeIntervalSince1970 * 1000)
//
//	}
//
//	override func didSimulatePhysics() {
//		let time = Int64(Date().timeIntervalSince1970 * 1000) - timeElapsed
//		if Int(score) % 10 == 0 {
//			print("Physics: \(time)")
//		}
//		timeElapsed = Int64(Date().timeIntervalSince1970 * 1000)
//	}
//
//	override func didFinishUpdate() {
//		let time = Int64(Date().timeIntervalSince1970 * 1000) - timeElapsed
//		if Int(score) % 10 == 0 {
//			print("Finish update: \(time)")
//			print(" - ")
//			print(" - ")
//		}
//		timeElapsed = Int64(Date().timeIntervalSince1970 * 1000)
//	}
	
	
	func setLevelSpeed(_ speed: Double) {
		self.levelSpeed = speed
		levelLayer?.speed = speed
		backgroundLayer3?.speed = speed * 0.7
		backgroundLayer2?.speed = speed * 0.5
		backgroundLayer1?.speed = speed * 0.2
		backgroundLayer0?.speed = speed + 2
	}
	
	
	
	func updateVertivalMovement2() {
		if player.isJumping() {
			if player.isGoingUp() {
				backgroundLayer1?.dy = 0.2
				backgroundLayer2?.dy = 0.3
				backgroundLayer3?.dy = 0.4
				levelLayer?.dy = 0.5
				backgroundLayer0?.dy = 0.5
			}
			else
				if player.isGoingDown() {
					backgroundLayer1?.dy = -0.2
					backgroundLayer2?.dy = -0.3
					backgroundLayer3?.dy = -0.4
					levelLayer?.dy = -0.5
					backgroundLayer0?.dy = -0.5
			}
		} else {
			backgroundLayer1?.dy = 0
			backgroundLayer2?.dy = 0
			backgroundLayer3?.dy = 0
			levelLayer?.dy = 0
			backgroundLayer0?.dy = 0
		}
	}
	
	
	// poruszanie planszą góra-dół
	func updateVerticalMovement() {
//		print("\(self.frame.midY), \(player.playerNode?.position.y)")
		
		let diff = abs(self.frame.midY - player.y)
		
		if player.y > self.frame.midY + 100 {
			backgroundLayer1?.dy = diff / 300
			backgroundLayer2?.dy = diff / 250
			backgroundLayer3?.dy = diff / 300
			levelLayer?.dy = diff / 90
		} else
			if player.y < self.frame.midY - 300 {
				backgroundLayer1?.dy = -diff / 300
				backgroundLayer2?.dy = -diff / 250
				backgroundLayer3?.dy = -diff / 300
				levelLayer?.dy = -diff / 90
				
			} else {
				DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(600), execute: {
					self.backgroundLayer1?.dy = 0
					self.backgroundLayer2?.dy = 0
					self.backgroundLayer3?.dy = 0
					self.levelLayer?.dy = 0
				})
				
		}
	}
	
	
	func death() {
		isDead = true
		player.playerNode?.removeFromParent()
		
		let sound = SKAction.playSoundFileNamed("death.wav", waitForCompletion: false)
		self.run(sound)
		
		let hiScore = UserDefaults.standard.integer(forKey: "HiScore")
		if Int(score) > hiScore {
			UserDefaults.standard.set(Int(score), forKey: "HiScore")
			self.addScoreAndSubmitToGC(Int(score))
		}

		self.levelLayer?.dy = 0
		self.backgroundLayer0?.dy = 0
		SKTAudio.sharedInstance().stopBackgroundMusic(2.0)

		
		if let spark = SKEmitterNode(fileNamed: "death.sks") {
			spark.position = player.playerNode!.position
			spark.zPosition = 200
			self.addChild(spark)
		}
		
		DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(2000), execute: {
			self.menu?.addDimPanel(fromAlpha: nil, toAlpha: 1.0, speed: 1, completion: {
				self.menu?.showMenu()
				self.scoreLabel?.alpha = 0.0
				self.isDead = false
				self.player.playerNode?.position = self.player.playerNode!.startPosition
				self.camera?.run(SKAction.scale(to: 1.0, duration: 0.1))
				self.player.clearState()
				ControllerManager.shared.clearState()

			})
			
			
		})
		
	}
	
	
	func addScoreAndSubmitToGC(_ score: Int) {
		if(GKLocalPlayer.local.isAuthenticated) {
			let bestScoreInt = GKScore(leaderboardIdentifier: hiScoreLeaderboard)
			bestScoreInt.value = Int64(score)
			GKScore.report([bestScoreInt]) { (error) in
				if error != nil {
					print(error!.localizedDescription)
				} else {
					print("Best Score submitted to your Leaderboard!")
				}
			}
		}
	}
	
	
	func playBounceSound() {
		let sound = SKAction.playSoundFileNamed("bounce.wav", waitForCompletion: false)
		self.run(sound)
	}
	
	
	func stopFlying() {
		//self.setLevelSpeed(4)
		self.physicsWorld.gravity = CGVector(dx: 0, dy: -9)
		self.player.stopFlying()
	}
	
	
	
	@objc func jump() {
		ControllerManager.shared.controllerState |= ControllerState.Jump.rawValue
	}
	
	@objc func duck() {
		ControllerManager.shared.controllerState |= ControllerState.Duck.rawValue
	}

	@objc func fly() {
		ControllerManager.shared.controllerState |= ControllerState.Fly.rawValue
		print("fly")
	}

	
}





extension GameScene: SKPhysicsContactDelegate {
	
	
	func didBegin(_ contact: SKPhysicsContact) {
		guard let nodeA = contact.bodyA.node, let nodeB = contact.bodyB.node else {
			return
		}

		if(contact.bodyA.node?.name == "Player" && (contact.bodyB.node?.name == "Trap" || contact.bodyB.node?.name == "DummyTrap")) {
			contactBetweenTrap(nodeA, nodeB)
		} else if((contact.bodyA.node?.name == "Trap" || contact.bodyA.node?.name == "DummyTrap") && contact.bodyB.node?.name == "Player") {
			contactBetweenTrap(nodeB, nodeA)
		}

		if(contact.bodyA.node?.name == "Player") {
			contactBetween(nodeA, nodeB)
		} else if(contact.bodyB.node?.name == "Player") {
			contactBetween(nodeB, nodeA)
		}


		
	}
	
	
	func contactBetween(_ playerNode:SKNode, _ object:SKNode)  {

		if isDead {
			return
		}

		let delta = Date.timeIntervalSinceReferenceDate - previousBounceTime

		if player.isDucking() && delta > 0.1 {
			playBounceSound()
			previousBounceTime = Date.timeIntervalSinceReferenceDate
		}
		
		
		if !self.player.isDucking() && delta > 0.3 {
			if !self.player.isGoingUp() {
				playBounceSound()
				self.player.bounce()
				previousBounceTime = Date.timeIntervalSinceReferenceDate
			}
			self.player.clearState()
			ControllerManager.shared.clearState()
		}

	}
	
	func contactBetweenTrap(_ playerNode:SKNode, _ object:SKNode)  {

		self.death()


		
	}


	
}




