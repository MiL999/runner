//
//  Bush.swift
//  Runner
//
//  Created by Miłosz on 8/19/19.
//  Copyright © 2019 Miłosz Duda. All rights reserved.
//



import UIKit
import SpriteKit

class Bush: SKSpriteNode {
	
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		
		if let texture = self.texture {
			
			self.name = "Bush"
			
			let swing = self.userData?["swing"] as? Int
			if let swing = swing {
				let rad = Double(swing) * .pi/180
				let action1 = SKAction.rotate(byAngle: CGFloat(rad), duration: 2.0)
				let action2 = SKAction.rotate(byAngle: -CGFloat(rad), duration: 2.0)
				action1.timingMode = .easeInEaseOut
				action2.timingMode = .easeInEaseOut
				let seq = SKAction.sequence([action1, action2])
				self.run(SKAction.repeatForever(seq))
			}
			
			let rotate = self.userData?["rotate"] as? Int
			if let rotate = rotate {
				let rad = Double(rotate) * .pi/180
				let action1 = SKAction.rotate(byAngle: CGFloat(rad), duration: 1.0)
				let seq = SKAction.sequence([action1])
				self.run(SKAction.repeatForever(seq))
			}
			
			
		}
	}
}




