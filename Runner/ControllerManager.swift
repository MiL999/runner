//
//  ControllerManager.swift
//  Runner
//
//  Created by Miłosz on 7/1/19.
//  Copyright © 2019 Miłosz Duda. All rights reserved.
//

import Foundation

enum ControllerState: UInt32 {
	case None = 0
	case Jump = 1
	case Left = 2
	case Right = 4
	case Duck = 8
	case Fly = 16
}


class ControllerManager {

	static let shared = ControllerManager()
	
	var controllerState: UInt32 = ControllerState.None.rawValue
	
	
	func clearState() {
		controllerState = ControllerState.None.rawValue
	}
	
	
}
