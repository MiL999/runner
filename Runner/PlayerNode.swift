//
//  PlayerController.swift
//  Runner
//
//  Created by Miłosz on 6/28/19.
//  Copyright © 2019 Miłosz Duda. All rights reserved.
//

import Foundation
import SpriteKit


enum PlayerTextureType {
	case Normal
	case Jump
	case Roll
	case Fly
}


class PlayerNode: SKSpriteNode {
	
	
	let playerTexture = SKTexture(imageNamed: "player1")
	let playerTextureJump = SKTexture(imageNamed: "player2")
	let playerTextureRoll = SKTexture(imageNamed: "player3")
	let playerTextureFly = SKTexture(imageNamed: "player4")
	var playerRunningFrames: [SKTexture] = []

	var startPosition = CGPoint(x:300, y:700)
	
	weak var player: Player?

	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
	
	init(_ player: Player) {
		super.init(texture: playerTexture, color: SKColor.clear, size: playerTexture.size())
		//buildPlayer()
		createPlayer()
		
		self.player = player
		
	}
	

	
	
	func createPlayer() {
		self.name = "Player"
		self.position = startPosition
//		self.texture = playerRunningFrames[0]
		self.texture = playerTexture // playerRunningFrames[0]
		self.size = CGSize(width: 36, height: 36)
//		self.size = CGSize(width: 100, height: 100)
		self.zPosition = 60
		//let body1 = SKPhysicsBody(rectangleOf: CGSize(width: 36, height: 36), center: CGPoint(x: 0, y: 00))
		let body2 = SKPhysicsBody(circleOfRadius: 18)
		self.physicsBody = SKPhysicsBody(bodies: [body2])
		self.physicsBody!.affectedByGravity = true
		self.physicsBody!.restitution = 0.0
		self.physicsBody!.linearDamping = 0.0
		self.physicsBody!.friction = 0.0
		self.physicsBody?.isDynamic = true
		self.physicsBody!.mass = 5
		self.physicsBody?.density = 1.0
		self.physicsBody!.allowsRotation = true
		self.physicsBody!.categoryBitMask = 1
		self.physicsBody?.collisionBitMask = 1
		self.physicsBody!.contactTestBitMask = 1
		

		
	}
	
	
	func buildPlayer() {
		
		let sheet = SpriteSheet(texture: SKTexture(imageNamed: "runner"), rows: 5, columns: 6, spacing: 1, margin: 1)
		var runFrames: [SKTexture] = []
		
		for y in (0...4).reversed() {
			for x in (0...5) {
				if let texture = sheet.textureForColumn(column: x, row: y) {
					runFrames.append(texture)
				}
			}
		}
		playerRunningFrames = runFrames
		
		self.run(SKAction.repeatForever(
			SKAction.animate(with: playerRunningFrames,
							 timePerFrame: 0.03,
							 resize: false,
							 restore: true)),
				 withKey:"walkingInPlaceBear")
	}
	
	
//	func buildPlayer() {
//
//		let sheet = SpriteSheet(texture: SKTexture(imageNamed: "runner"), rows: 5, columns: 6, spacing: 1, margin: 1)
//
//		//	let bearAnimatedAtlas = SKTextureAtlas(named: "Runner")
//		//	var runFrames: [SKTexture] = []
//
//		let numImages = 30 //bearAnimatedAtlas.textureNames.count
//		for i in (0...numImages-1).reversed() {
//			let bearTextureName = "Runner-\(i)"
//			runFrames.append(bearAnimatedAtlas.textureNamed(bearTextureName))
//		}
//		playerRunningFrames = runFrames
//
//	}
	
	func duck() {
		let squashOut = SKAction.scaleY(to: 1.0, duration: 0.2)
		let s = SKAction.sequence([squashOut])
		self.run(s)

		let rotate = SKAction.rotate(byAngle: -18, duration: 2.2)
		rotate.timingMode = .easeOut
		self.run(rotate)
		let wait = SKAction.wait(forDuration: 1.8)
		let completion = SKAction.run(
		{
			self.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 10))
			self.stopDucking()
		})
		let seq = SKAction.sequence([wait, completion])
		self.run(seq)
		self.setPlayerTexture(.Roll)
	}
	
	func stopDucking() {
		self.physicsBody?.restitution = 0.0
		
		self.removeAction(forKey: "duck")
		let rotate = SKAction.rotate(toAngle: 0.0, duration: 1.0, shortestUnitArc: false)
		self.run(rotate)
		self.setPlayerTexture(.Normal)
		
		self.player?.playerState &= ~PlayerState.Ducking.rawValue
	}
	
	func jump() {
		setPlayerTexture(.Jump)
		let squashIn = SKAction.scaleY(to: 1.2, duration: 0.1)
		let seq = SKAction.sequence([squashIn])
		self.run(seq)

	}
	
	func bounce() {
		setPlayerTexture(.Normal)
		let squashIn = SKAction.scaleY(to: 0.6, duration: 0.02)
		let seq = SKAction.sequence([squashIn])
		self.run(seq, completion: {
			self.physicsBody?.velocity = CGVector.zero
			self.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 20))
			let squashOut = SKAction.scaleY(to: 1.0, duration: 0.2)
			self.run(squashOut)
		})
		

	}
	
	func fly() {
		setPlayerTexture(.Fly)
		let z = SKAction.scale(to: 1.0, duration: 0.01)
		self.run(z)
		let squashIn = SKAction.scale(to: 0.8, duration: 0.2)
		let squashOut = SKAction.scale(to: 1.8, duration: 0.01)
		let s = SKAction.scale(to: 1.6, duration: 0.5)
		let seq = SKAction.sequence([squashIn, squashOut, s])
		self.run(seq)
	}

	func stopFlying() {
		setPlayerTexture(.Normal)
		let z = SKAction.scale(to: 1.0, duration: 0.02)
		self.run(z)
	}

	
	func setPlayerTexture(_ type: PlayerTextureType) {
		switch type {
		case .Normal:
			self.texture = playerTexture
		case .Jump:
			self.texture = playerTextureJump
		case .Roll:
			self.texture = playerTextureRoll
		case .Fly:
			self.texture = playerTextureFly
		}
	}
	
}
